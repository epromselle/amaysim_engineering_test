package com.roselleelefante.android.amaysim.models;

/**
 * Created by roselleelefante on 06/06/2017.
 */

public class Product {

    private String id;
    private String name;
    private long includedData;
    private long includedCredit;
    private long includedInternationalTalk;
    private boolean isUnlimitedText;
    private boolean isUnlimitedTalk;
    private boolean isUnlimitedInternationalText;
    private boolean isUnlimitedInternationalTalk;
    private long price;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getIncludedData() {
        return includedData;
    }

    public void setIncludedData(long includedData) {
        this.includedData = includedData;
    }

    public long getIncludedCredit() {
        return includedCredit;
    }

    public void setIncludedCredit(long includedCredit) {
        this.includedCredit = includedCredit;
    }

    public long getIncludedInternationalTalk() {
        return includedInternationalTalk;
    }

    public void setIncludedInternationalTalk(long includedInternationalTalk) {
        this.includedInternationalTalk = includedInternationalTalk;
    }

    public boolean isUnlimitedText() {
        return isUnlimitedText;
    }

    public void setUnlimitedText(boolean unlimitedText) {
        isUnlimitedText = unlimitedText;
    }

    public boolean isUnlimitedTalk() {
        return isUnlimitedTalk;
    }

    public void setUnlimitedTalk(boolean unlimitedTalk) {
        isUnlimitedTalk = unlimitedTalk;
    }

    public boolean isUnlimitedInternationalText() {
        return isUnlimitedInternationalText;
    }

    public void setUnlimitedInternationalText(boolean unlimitedInternationalText) {
        isUnlimitedInternationalText = unlimitedInternationalText;
    }

    public boolean isUnlimitedInternationalTalk() {
        return isUnlimitedInternationalTalk;
    }

    public void setUnlimitedInternationalTalk(boolean unlimitedInternationalTalk) {
        isUnlimitedInternationalTalk = unlimitedInternationalTalk;
    }

    public long getPrice() {
        return price;
    }

    public void setPrice(long price) {
        this.price = price;
    }
}
