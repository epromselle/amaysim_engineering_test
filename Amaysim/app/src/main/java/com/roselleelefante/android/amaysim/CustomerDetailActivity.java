package com.roselleelefante.android.amaysim;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import com.roselleelefante.android.amaysim.models.Account;
import com.roselleelefante.android.amaysim.models.Product;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by roselleelefante on 06/06/2017.
 * Detail screen for cusotmer's personal and subscription data
 */

public class CustomerDetailActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer_detail);

        Account account = Account.getInstance();

        TextView overviewLabel = (TextView) findViewById(R.id.overview_label);
        TextView remDataBalTextView = (TextView) findViewById(R.id.rem_data_bal_tv);
        TextView creditBalTextView = (TextView) findViewById(R.id.credit_bal_tv);
        TextView expiryDateTextView = (TextView) findViewById(R.id.expiry_date_tv);
        TextView autoRenewalTextView = (TextView) findViewById(R.id.auto_renewal_tv);
        TextView planNameTv = (TextView) findViewById(R.id.plan_name_tv);
        TextView dataTv = (TextView) findViewById(R.id.included_data_tv);
        TextView creditTv = (TextView) findViewById(R.id.included_credit_tv);
        TextView intlTalkTv = (TextView) findViewById(R.id.intl_talk_tv);
        TextView unliTextTv = (TextView) findViewById(R.id.unlimited_text);
        TextView unliTalkTv = (TextView) findViewById(R.id.unlimited_talk);
        TextView unliIntlTextTv = (TextView) findViewById(R.id.unlimited_intl_text);
        TextView unliIntlTalkTv = (TextView) findViewById(R.id.unlimited_intl_talk);
        TextView priceTv = (TextView) findViewById(R.id.price_tv);

        String labelStr = "Hi " + account.getFirstName() + "! Here's an overview of your Amaysim account.";
        overviewLabel.setText(labelStr);

        long remData = account.getService().getSubscription().getIncludedDataBalance();
        double dataInGb = remData / 1024.00;
        remDataBalTextView.setText(String.format("%.2f", dataInGb) + " GB Left");

        long creditBal = account.getService().getSubscription().getIncludedCreditBalance();
        creditBalTextView.setText(String.valueOf(creditBal));

        String expiryDate = account.getService().getSubscription().getExpiryDate();
        String date = parseDate(expiryDate);
        expiryDateTextView.setText(date);

        boolean isAutoRenewal = account.getService().getSubscription().isAutoRenewal();
        String isAutoRenewalStr = isAutoRenewal ? "Yes" : "No";
        autoRenewalTextView.setText(isAutoRenewalStr);

        planNameTv.setText(account.getService().getSubscription().getProduct().getName());

        long data = account.getService().getSubscription().getProduct().getIncludedData();
        double dataGb = data / 1024.00;
        dataTv.setText(String.format("%.2f", dataGb) + " GB");

        long credit = account.getService().getSubscription().getProduct().getIncludedCredit();
        creditTv.setText(String.valueOf(credit));

        long intlTalk = account.getService().getSubscription().getProduct().getIncludedInternationalTalk();
        intlTalkTv.setText(String.valueOf(intlTalk));

        Product product = account.getService().getSubscription().getProduct();
        if (product.isUnlimitedText()) {
            unliTextTv.setVisibility(View.VISIBLE);
        }
        if (product.isUnlimitedTalk()) {
            unliTalkTv.setVisibility(View.VISIBLE);
        }
        if (product.isUnlimitedInternationalText()) {
            unliIntlTextTv.setVisibility(View.VISIBLE);
        }
        if (product.isUnlimitedInternationalTalk()) {
            unliIntlTalkTv.setVisibility(View.VISIBLE);
        }

        long price = product.getPrice();
        double priceInDollar = price / 100.00;
        priceTv.setText(String.format("%.2f", priceInDollar) + " $");

        TextView msnTv = (TextView) findViewById(R.id.msn_tv);
        TextView accountNameTv = (TextView) findViewById(R.id.account_name_tv);
        TextView mobileNumTv = (TextView) findViewById(R.id.mobile_num_tv);
        TextView emailTv = (TextView) findViewById(R.id.email_add_tv);
        TextView emailVerifiedTv = (TextView) findViewById(R.id.email_verified_tv);
        TextView emailSubscriptionTv = (TextView) findViewById(R.id.email_subscription_tv);
        TextView paymentTypeTv = (TextView) findViewById(R.id.payment_type_tv);

        msnTv.setText(account.getService().getMsn());
        accountNameTv.setText(account.getFirstName() + " " + account.getLastName());
        mobileNumTv.setText(account.getContactNumber());
        emailTv.setText(account.getEmailAddress());

        String emailVerifiedStr = account.isEmailAddressVerified() ? "Yes" : "No";
        emailVerifiedTv.setText(emailVerifiedStr);

        String emailSubscriptionStr = account.isEmailSubscriptionStatus() ? "Yes" : "No";
        emailSubscriptionTv.setText(emailSubscriptionStr);

        paymentTypeTv.setText(account.getPaymentType());
    }

    public String parseDate(String dateStr) {
        String inputPattern = "yyyy-MM-dd";
        String outputPattern = "MMM dd, yyyy";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(dateStr);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }
}
