package com.roselleelefante.android.amaysim;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.roselleelefante.android.amaysim.models.Account;

/**
 * Created by roselleelefante on 06/06/2017.
 * Screen for logging in of accounts
 */

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    Context mContext;
    private EditText loginEditText;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        mContext = this;

        loginEditText = (EditText) findViewById(R.id.login_edit_text);
        Button loginButton = (Button) findViewById(R.id.login_button);
        loginButton.setOnClickListener(this);

        Account.fetchAccount(mContext);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.login_button:
                String loginMsn = loginEditText.getText().toString();
                Account account = Account.getInstance();
                if (loginMsn.equalsIgnoreCase(account.getService().getMsn())) {
                    Intent intent = new Intent(mContext, SplashActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                } else {
                    Toast.makeText(this, R.string.message_invalid_login, Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }
}
