package com.roselleelefante.android.amaysim.models;

import android.content.Context;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;

/**
 * Created by roselleelefante on 06/06/2017.
 */

public class Account {

    private static Account account = null;
    private String id;
    private String paymentType;
    private String unbilledCharges;
    private String nextBillingDate;
    private String title;
    private String firstName;
    private String lastName;
    private String dateOfBirth;
    private String contactNumber;
    private String emailAddress;
    private boolean isEmailAddressVerified;
    private boolean isEmailSubscriptionStatus;
    private Services service;

    private Account() {

    }

    public static Account getInstance() {
        if (account == null) {
            account = new Account();
        }
        return account;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public String getUnbilledCharges() {
        return unbilledCharges;
    }

    public void setUnbilledCharges(String unbilledCharges) {
        this.unbilledCharges = unbilledCharges;
    }

    public String getNextBillingDate() {
        return nextBillingDate;
    }

    public void setNextBillingDate(String nextBillingDate) {
        this.nextBillingDate = nextBillingDate;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getContactNumber() {
        return contactNumber;
    }

    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public boolean isEmailAddressVerified() {
        return isEmailAddressVerified;
    }

    public void setEmailAddressVerified(boolean emailAddressVerified) {
        isEmailAddressVerified = emailAddressVerified;
    }

    public boolean isEmailSubscriptionStatus() {
        return isEmailSubscriptionStatus;
    }

    public void setEmailSubscriptionStatus(boolean emailSubscriptionStatus) {
        isEmailSubscriptionStatus = emailSubscriptionStatus;
    }

    public Services getService() {
        return service;
    }

    public void setService(Services service) {
        this.service = service;
    }

    public static Account fetchAccount(Context context) {
        try {
            JSONObject jsonObject = new JSONObject(loadJsonFromAsset(context));
            JSONObject acctDataObj = jsonObject.optJSONObject("data");
            if (acctDataObj != null) {
                if (acctDataObj.optString("type").equalsIgnoreCase("accounts")) {
                    Account account = getInstance();
                    account.setId(acctDataObj.optString("id"));

                    JSONObject acctAttribObj = acctDataObj.optJSONObject("attributes");
                    if (acctAttribObj != null) {
                        account.setPaymentType(acctAttribObj.optString("payment-type"));
                        account.setUnbilledCharges(acctAttribObj.optString("unbilled-charges"));
                        account.setNextBillingDate(acctAttribObj.optString("next-billing-date"));
                        account.setTitle(acctAttribObj.optString("title"));
                        account.setFirstName(acctAttribObj.optString("first-name"));
                        account.setLastName(acctAttribObj.optString("last-name"));
                        account.setDateOfBirth(acctAttribObj.optString("date-of-birth"));
                        account.setContactNumber(acctAttribObj.optString("contact-number"));
                        account.setEmailAddress(acctAttribObj.optString("email-address"));
                        account.setEmailAddressVerified(acctAttribObj.optBoolean("email-address-verified"));
                        account.setEmailSubscriptionStatus(acctAttribObj.optBoolean("email-subscription-status"));
                    }

                    JSONArray includedArray = jsonObject.optJSONArray("included");
                    Services service = null;
                    Subscription subscription = null;
                    Product product = null;
                    if (includedArray != null) {
                        for (int i=0; i<includedArray.length(); i++) {
                            JSONObject inclObj = includedArray.optJSONObject(i);
                            if (inclObj != null) {
                                String type = inclObj.optString("type");
                                String id = inclObj.optString("id");
                                JSONObject attribObj = inclObj.optJSONObject("attributes");
                                if (type.equalsIgnoreCase("services")) {
                                    service = new Services();
                                    service.setId(id);
                                    if (attribObj != null) {
                                        service.setMsn(attribObj.optString("msn"));
                                        service.setCredit(attribObj.optInt("credit"));
                                        service.setCreditExpiry(attribObj.optString("credit-expiry"));
                                        service.setDataUsageThreshold(attribObj.optBoolean("data-usage-threshold"));
                                    }
                                } else if (type.equalsIgnoreCase("subscriptions")) {
                                    subscription = new Subscription();
                                    subscription.setId(id);
                                    if (attribObj != null) {
                                        subscription.setIncludedDataBalance(attribObj.optLong("included-data-balance"));
                                        subscription.setIncludedCreditBalance(attribObj.optLong("included-credit-balance"));
                                        subscription.setIncludedRollOverCreditBalance(attribObj.optLong("included-rollover-credit-balance"));
                                        subscription.setIncludedRollOverDataBalance(attribObj.optLong("included-rollover-data-balance"));
                                        subscription.setIncludedInternationalTalkBalance(attribObj.optLong("included-international-talk-balance"));
                                        subscription.setExpiryDate(attribObj.optString("expiry-date"));
                                        subscription.setAutoRenewal(attribObj.optBoolean("auto-renewal"));
                                        subscription.setPrimarySubscription(attribObj.optBoolean("primary-subscription"));
                                    }
                                } else if (type.equalsIgnoreCase("products")) {
                                    product = new Product();
                                    product.setId(id);
                                    if (attribObj != null) {
                                        product.setName(attribObj.optString("name"));
                                        product.setIncludedData(attribObj.optLong("included-data"));
                                        product.setIncludedCredit(attribObj.optLong("included-credit"));
                                        product.setIncludedInternationalTalk(attribObj.optLong("included-international-talk"));
                                        product.setUnlimitedText(attribObj.optBoolean("unlimited-text"));
                                        product.setUnlimitedTalk(attribObj.optBoolean("unlimited-talk"));
                                        product.setUnlimitedInternationalText(attribObj.optBoolean("unlimited-international-text"));
                                        product.setUnlimitedInternationalTalk(attribObj.optBoolean("unlimited-international-talk"));
                                        product.setPrice(attribObj.optLong("price"));
                                    }
                                }
                            }
                        }
                    }

                    if (subscription != null) {
                        subscription.setProduct(product);
                    }
                    if (service != null) {
                        service.setSubscription(subscription);
                    }
                    account.setService(service);

                    return account;
                }
            }

            return null;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    private static String loadJsonFromAsset(Context context) {
        String json = null;
        try {
            InputStream is = context.getAssets().open("collection.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }

        return json;
    }
}
