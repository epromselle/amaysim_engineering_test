package com.roselleelefante.android.amaysim.models;

/**
 * Created by roselleelefante on 06/06/2017.
 */

public class Services {

    private String id;
    private String msn;
    private int credit;
    private String creditExpiry;
    private boolean isDataUsageThreshold;
    private Subscription subscription;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMsn() {
        return msn;
    }

    public void setMsn(String msn) {
        this.msn = msn;
    }

    public int getCredit() {
        return credit;
    }

    public void setCredit(int credit) {
        this.credit = credit;
    }

    public String getCreditExpiry() {
        return creditExpiry;
    }

    public void setCreditExpiry(String creditExpiry) {
        this.creditExpiry = creditExpiry;
    }

    public boolean isDataUsageThreshold() {
        return isDataUsageThreshold;
    }

    public void setDataUsageThreshold(boolean dataUsageThreshold) {
        isDataUsageThreshold = dataUsageThreshold;
    }

    public Subscription getSubscription() {
        return subscription;
    }

    public void setSubscription(Subscription subscription) {
        this.subscription = subscription;
    }
}
