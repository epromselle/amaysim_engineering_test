package com.roselleelefante.android.amaysim.models;

/**
 * Created by roselleelefante on 06/06/2017.
 */

public class Subscription {

    private String id;
    private long includedDataBalance;
    private long includedCreditBalance;
    private long includedRollOverCreditBalance;
    private long includedRollOverDataBalance;
    private long includedInternationalTalkBalance;
    private String expiryDate;
    private boolean isAutoRenewal;
    private boolean isPrimarySubscription;
    private Product product;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public long getIncludedDataBalance() {
        return includedDataBalance;
    }

    public void setIncludedDataBalance(long includedDataBalance) {
        this.includedDataBalance = includedDataBalance;
    }

    public long getIncludedCreditBalance() {
        return includedCreditBalance;
    }

    public void setIncludedCreditBalance(long includedCreditBalance) {
        this.includedCreditBalance = includedCreditBalance;
    }

    public long getIncludedRollOverCreditBalance() {
        return includedRollOverCreditBalance;
    }

    public void setIncludedRollOverCreditBalance(long includedRollOverCreditBalance) {
        this.includedRollOverCreditBalance = includedRollOverCreditBalance;
    }

    public long getIncludedRollOverDataBalance() {
        return includedRollOverDataBalance;
    }

    public void setIncludedRollOverDataBalance(long includedRollOverDataBalance) {
        this.includedRollOverDataBalance = includedRollOverDataBalance;
    }

    public long getIncludedInternationalTalkBalance() {
        return includedInternationalTalkBalance;
    }

    public void setIncludedInternationalTalkBalance(long includedInternationalTalkBalance) {
        this.includedInternationalTalkBalance = includedInternationalTalkBalance;
    }

    public String getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(String expiryDate) {
        this.expiryDate = expiryDate;
    }

    public boolean isAutoRenewal() {
        return isAutoRenewal;
    }

    public void setAutoRenewal(boolean autoRenewal) {
        isAutoRenewal = autoRenewal;
    }

    public boolean isPrimarySubscription() {
        return isPrimarySubscription;
    }

    public void setPrimarySubscription(boolean primarySubscription) {
        isPrimarySubscription = primarySubscription;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }
}
